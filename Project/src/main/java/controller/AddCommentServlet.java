package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.CommentDao;
import model.Comment;

/**
 * Servlet implementation class Comment
 */
@WebServlet("/AddCommentServlet")
public class AddCommentServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AddCommentServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    // (はてなの後ろ URLみるとID=1とかなってる)
    String threadId = request.getParameter("id");

    // カテゴリー一覧情報を取得
    CommentDao commentDao = new CommentDao();
    List<Comment> commentList = commentDao.findAll(threadId);

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("commentList", commentList);
    request.setAttribute("threadId", threadId);


    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addComment.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String theadIdStr = request.getParameter("threadId");
    String user_name = request.getParameter("user_name");
    String text = request.getParameter("text");

    CommentDao commentDao = new CommentDao();
    if (user_name.equals("") || text.equals("")) {
      request.setAttribute("errMsg", "何かが入ってません！");


      List<Comment> commentList = commentDao.findAll(theadIdStr);

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("commentList", commentList);

      // jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addComment.jsp");
      dispatcher.forward(request, response);
      return;
    }
    int threadId = Integer.parseInt(theadIdStr);
    commentDao.create(threadId, user_name, text);

    // コメントサーブレットにリダイレクト
    response.sendRedirect("AddCommentServlet?id=" + threadId);

  }

}//

package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.ThreadDao;
import model.Thread;

/**
 * Servlet implementation class Thread
 */
@WebServlet("/AddThreadServlet")
public class AddThreadServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public AddThreadServlet() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // カテゴリー一覧情報を取得
    ThreadDao threadDao = new ThreadDao();
    List<Thread> threadList = threadDao.findAll();

    // リクエストスコープにユーザ一覧情報をセット
    request.setAttribute("threadList", threadList);

    // フォワード
    RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addThread.jsp");
    dispatcher.forward(request, response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.setCharacterEncoding("UTF-8");

    // リクエストパラメータの入力項目を取得
    String title = request.getParameter("title");
    String user_name = request.getParameter("user_name");
    String text = request.getParameter("text");

    ThreadDao threadDao = new ThreadDao();
    if (title.equals("") || user_name.equals("") || text.equals("")) {
      request.setAttribute("errMsg", "何かが入ってません！");
      // カテゴリー一覧情報を取得
      List<Thread> threadList = threadDao.findAll();


      // リクエストスコープに一覧情報をセット
      request.setAttribute("threadList", threadList);

      // jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/addThread.jsp");
      dispatcher.forward(request, response);
      return;
    }

    threadDao.create(title, user_name, text);
    // サーブレットにリダイレクト
    response.sendRedirect("AddThreadServlet");



  }

}

package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.ThreadDao;
import model.Thread;

/**
 * Servlet implementation class SearchThreadServlet
 */
@WebServlet("/SearchThreadServlet")
public class SearchThreadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchThreadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // カテゴリー一覧情報を取得
      ThreadDao threadDao = new ThreadDao();
      List<Thread> threadList = threadDao.findAll();

      // リクエストスコープにユーザ一覧情報をセット
      request.setAttribute("threadList", threadList);

      // フォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchThread.jsp");
      dispatcher.forward(request, response);
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String suretai = request.getParameter("suretai");
      // リクエストスコープに一覧情報をセット
      ThreadDao threadDao = new ThreadDao();
      if (!suretai.equals("")) {
        List<Thread> threadList = threadDao.search(suretai);// 詰める// daoに飛ぶ
        request.setAttribute("threadList", threadList);

      }
      // jspにフォワード
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/searchThread.jsp");
      dispatcher.forward(request, response);


	}
}

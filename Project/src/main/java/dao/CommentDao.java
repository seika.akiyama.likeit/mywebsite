package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Comment;

public class CommentDao {


  // 全てのコメントを取得する
  public List<Comment> findAll(String threadId) {
    Connection conn = null;
    List<Comment> commentList = new ArrayList<Comment>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM comment WHERE thread_id =? ORDER BY create_date DESC;";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, threadId);
      ResultSet rs = pStmt.executeQuery();

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String text = rs.getString("text");
        String user_name = rs.getString("user_name");
        Timestamp create_date = rs.getTimestamp("create_date");

        Comment comment = new Comment(id, text, user_name, create_date);

        commentList.add(comment);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return commentList;
  }


  //コメントの作成できる
  public void create(int threadId, String user_name, String text) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql =
          "INSERT INTO comment(thread_id,user_name,text,create_date) VALUES (?,?,?,now());";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, threadId);
      pStmt.setString(2, user_name);
      pStmt.setString(3, text);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }


}// 固定

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Thread;

public class ThreadDao {



  // 全てのスレッドを取得する
  public List<Thread> findAll() {
    Connection conn = null;
    List<Thread> threadList = new ArrayList<Thread>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM thread ORDER BY create_date DESC;";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // インスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String title = rs.getString("title");
        String text = rs.getString("text");
        String user_name = rs.getString("user_name");
        Timestamp create_date = rs.getTimestamp("create_date");

        Thread thread = new Thread(id, title, text, user_name, create_date);

        threadList.add(thread);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return threadList;
  }


  // スレッドの作成できる
  public void create(String title, String user_name, String text) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "INSERT INTO thread(title,user_name,text,create_date) VALUES (?,?,?,now());";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, title);
      pStmt.setString(2, user_name);
      pStmt.setString(3, text);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();

    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();

        }
      }
    }
  }


  // スレタイ検索
  public List<Thread> search(String title) {
    Connection conn = null;

    ArrayList<Thread> threadList = new ArrayList<Thread>();
    ArrayList<String> strList = new ArrayList<String>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();



      // 基準
      String sql = "SELECT * FROM thread WHERE";

      // login_id =? AND name=? AND birth_date=? AND birth_date=?
      StringBuilder StringBuilder = new StringBuilder(sql);

      if (!title.equals("")) {
        StringBuilder.append(" title LIKE ? ORDER BY create_date DESC");
        strList.add("%" + title + "%");
      }

      PreparedStatement pStmt = conn.prepareStatement(StringBuilder.toString());
      for (int i = 0; i < strList.size(); i++) {
        pStmt.setString(i + 1, strList.get(i));
      }

      ResultSet rs = pStmt.executeQuery();

      // インスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String title1 = rs.getString("title");
        String text = rs.getString("text");
        String user_name = rs.getString("user_name");
        Timestamp create_date = rs.getTimestamp("create_date");

        Thread thread = new Thread(id, title1, text, user_name, create_date);

        threadList.add(thread);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return threadList;
  }

}// 固定

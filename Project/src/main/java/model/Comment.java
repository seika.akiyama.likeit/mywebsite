package model;

import java.io.Serializable;
import java.sql.Timestamp;

public class Comment implements Serializable {
  private int id;
  private String text;
  private String user_name;
  private Timestamp createDate;



  public Comment(int id, String text, String user_name, Timestamp createDate) {
    super();
    this.id = id;
    this.text = text;
    this.user_name = user_name;
    this.createDate = createDate;
  }


  public Comment() {
    super();
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public String getText() {
    return text;
  }


  public void setText(String text) {
    this.text = text;
  }


  public String getUser_name() {
    return user_name;
  }


  public void setUser_name(String user_name) {
    this.user_name = user_name;
  }


  public Timestamp getCreateDate() {
    return createDate;
  }


  public void setCreateDate(Timestamp createDate) {
    this.createDate = createDate;
  }



}

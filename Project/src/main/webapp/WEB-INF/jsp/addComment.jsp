<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DataFormat用 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>スレッド一覧</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/common.css" rel="stylesheet">
<link href="css/comment.css" rel="stylesheet">
</head>

<body>

	<div class="col-12">
		<div class="container">
			<div class="parent">
				<br> <br> <br>
				<div class="NEW_THREAD"
					style="z-index: 1; width: 93%; margin: 0.5em auto 0.75em auto; padding: 0.75em; border: 1px solid #333; color: #333; border-radius: 0.75em/0.75em; background: #CFC; font-size: 1.00em;">
					<h3>
						<span class="common">コメント一覧</span>
					</h3>
					<br>

					<c:forEach var="comment" items="${commentList}">
						<tr>
							<td>${comment.user_name}</td>
							<td>${comment.text}</td>
							<td><fmt:formatDate value="${comment.createDate}"
									pattern="yyyy/MM/dd HH:mm:ss" /><br></td>


						</tr>
					</c:forEach>
				</div>

				<br>
				<div class="NEW_THREAD"
					style="z-index: 1; width: 93%; margin: 0.5em auto 0.75em auto; padding: 0.75em; border: 1px solid #333; color: #333; border-radius: 0.75em/0.75em; background: #CFC; font-size: 1.00em;">
					<form method=POST action="AddCommentServlet">
						<a name="new_thread"></a>
						<h3>
							<span class="common">コメント書き込み</span>
						</h3>
						<!-- エラー処理 start -->
						<c:if test="${errMsg != null}">
							<div class="alert alert-danger" role="alert">${errMsg}</div>
						</c:if>

						<p style="margin: 0 0 0 2em; font-size: 0.75em;">
							<br> 名前：<input type="text" name="user_name"
								style="width: 24em;"><br>
							<textarea
								style="min-width: 40em; height: 10.0em; word-wrap: break-word;"
								rows="4" cols="12" name="text"></textarea>
							<input type="hidden" name="threadId" value="${threadId}">
							<input type="submit" value="書き込む" name="submit"><br>


						</p>
					</form>

					<div class="col-xs-4">
						<a href="AddThreadServlet">戻る</a>
					</div>
				</div>
			</div>
			<br> <br>
		</div>
	</div>


</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DataFormat用 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>掲示板</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/common.css" rel="stylesheet">
<link href="css/thread.css" rel="stylesheet">
</head>

<body>

	<div class="col-12">
		<div class="container">
			<div class="parent">

				<br> <br> <br> <a href="SearchThreadServlet" class="btn-circle-stitch">検索ページへ</a>

				<br> <br> <br>


				<div class="NEW_THREAD"
					style="z-index: 1; width: 93%; margin: 0.5em auto 0.75em auto; padding: 0.75em; border: 1px solid #333; color: #333; border-radius: 0.75em/0.75em; background: #CFC; font-size: 1.00em;">
					<h3>
						<span class="common">スレッド一覧</span>
					</h3>

					<c:forEach var="thread" items="${threadList}">
						<a href="AddCommentServlet?id=${thread.id}">
							<tr>
								<td>${thread.title}</td>
								<td>${thread.user_name}</td>
								<td>${thread.text}</td>
								<td><fmt:formatDate value="${thread.createDate}"
										pattern="yyyy/MM/dd HH:mm:ss" /><br></td>


							</tr>
					</c:forEach>
					</a>

				</div>
				<br>
				<div class="NEW_THREAD"
					style="z-index: 1; width: 93%; margin: 0.5em auto 0.75em auto; padding: 0.75em; border: 1px solid #333; color: #333; border-radius: 0.75em/0.75em; background: #CFC; font-size: 1.00em;">

					<a name="new_thread"></a>
					<h3>
						<span class="common">新規スレッド作成</span>
					</h3>
					<!-- エラー処理 start -->
					<c:if test="${errMsg != null}">
						<div class="alert alert-danger" role="alert">${errMsg}</div>
					</c:if>
					<form action="AddThreadServlet" method="post">
						<p style="margin: 0 0 0 2em; font-size: 0.75em;">
							<br> スレッドタイトル：<input type="text" name="title"
								style="width: 24em;"><br> 　名　　　 前　　：<input type="text"
								name="user_name" style="width: 24em;"><br>
							<textarea
								style="min-width: 40em; height: 10.0em; word-wrap: break-word;"
								rows="4" cols="12" name="text"></textarea>

							<input type="submit" value="作る" name="submit"><br>

						</p>
					</form>
				</div>
			</div>
		</div>
	</div>
	<br>
	<br>
</body>

</html>
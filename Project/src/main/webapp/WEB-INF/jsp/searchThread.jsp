<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- DataFormat用 -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>検索</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/common.css" rel="stylesheet">
<link href="css/thread.css" rel="stylesheet">
</head>
<body>

	<div class="col-12">
		<div class="container">
			<div class="parent">

				<br> <br> <br>
				<form action="SearchThreadServlet" method="post">


					<input type="text" name="suretai" placeholder="スレタイ検索"
						style="width: 20em; height: 2.0em; padding: 0.5em;">
					<button type="submit" class="btn-stitch">search</button>
					<br> <br> <br>
				</form>

				<div class="NEW_THREAD"
					style="z-index: 1; width: 93%; margin: 0.5em auto 0.75em auto; padding: 0.75em; border: 1px solid #333; color: #333; border-radius: 0.75em/0.75em; background: #CFC; font-size: 1.00em;">
					<h3>
						<span class="common">スレッド一覧</span>
					</h3>

					<c:forEach var="thread" items="${threadList}">
						<a href="AddCommentServlet">
							<tr>
								<td>${thread.title}</td>
								<td>${thread.user_name}</td>
								<td>${thread.text}</td>
								<td><fmt:formatDate value="${thread.createDate}"
										pattern="yyyy/MM/dd HH:mm:ss" /><br></td>


							</tr>
					</c:forEach>
					</a>
				</div>
				<br> <a href="AddThreadServlet">戻る</a> <br> <br>
			</div>
		</div>
	</div>

</body>
</html>
CREATE DATABASE keiziban DEFAULT CHARACTER SET utf8;
USE keiziban;


-- テーブルの構造 `thread`
CREATE TABLE thread (
id SERIAL PRIMARY KEY AUTO_INCREMENT,
title varchar(255) UNIQUE NOT NULL,
text varchar(255) NOT NULL,
user_name varchar(255) NOT NULL,
create_date DATETIME NOT NULL);

-- テーブルのデータ `thread`
INSERT INTO thread(id,title,text,user_name,create_date) VALUES (1,'テストタイトル','テスト本文''admin',now());


-- テーブルの構造 `comment`
CREATE TABLE comment (
id SERIAL PRIMARY KEY AUTO_INCREMENT,
text varchar(255) NOT NULL,
user_name varchar(255) NOT NULL,
create_date DATETIME NOT NULL,);

-- テーブルのデータ `comment`
INSERT INTO comment(id,text,user_name,create_date) VALUES (1,'テスト本文','admin',now());



----------
USE keiziban;

